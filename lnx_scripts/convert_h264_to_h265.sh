#!/bin/sh
# --------------------------------------------------------------------------------------------------
# copy all stream to h265 mkv and process video only
echo "[$(date)]  H264_to_H265 Converter Script:  CRF 18 with UFAST preset." > ./convert_h264_to_h265.log
echo "[$(date)]  Start conversion for DIR:  $1" >> ./convert_h264_to_h265.log
{
        # Delay for catch current PID
        sleep 3
        # Some works to argument shell
        argdir="$1"
        # Loop files into current directory
        for filemkv in $argdir/*.mkv
        do
                # Get basename
                filename=$(basename "$filemkv" .mkv)
                echo "[$(date)]  Processing file:  $filemkv"  >> ./convert_h264_to_h265.log
                # Check if file is already HVEC or not
                ishvec=$(ffprobe "$filemkv" 2>&1 >/dev/null | grep "Stream.*Video" | sed -e 's/.*Video: //' -e 's/[, ].*//')
                # Run block of code in background
                # ----------------------------------------------------------------------------------
                patternstr="h264"
                if echo "$ishvec" | grep -q "$patternstr";
                then
                        # Conversion with QSV-Intel (hevc_qsv -load_plugin hevc_hw -preset:v faster) and copy of all subs and audio
                        # ffmpeg -loglevel warning -i "$filemkv" -y -map 0 -c copy -c:v hevc_qsv -preset:v faster -crf 18 "$argdir/$filename [H265].mkv" >> ./convert_h264_to_h265.log
                        # Conversion and copy of all subs and audio
                        ffmpeg -loglevel warning -i "$filemkv" -y -map 0 -c copy -c:v libx265 -preset ultrafast -crf 18 "$argdir/$filename [H265].mkv" >> ./convert_h264_to_h265.log
                        echo "[$(date)]  End conversion to H265 of previous file." >> ./convert_h264_to_h265.log
                else
                        echo "[$(date)]  H264 encoded file not found! Already H265 maybe, skip current..." >> ./convert_h264_to_h265.log
                fi
                echo "[$(date)]  ----------------------------------------------------------------------------------------------------" >> ./convert_h264_to_h265.log
                #-----------------------------------------------------------------------------------
                if echo "$ishvec" | grep -q "$patternstr";
                then
                        # CPU sleep after conversion
                        sleep 60
                fi
        done
        echo "[$(date)]  All conversions completed." >> ./convert_h264_to_h265.log
} &
# --------------------------------------------------------------------------------------------------
echo "[$(date)]  Read 'convert_h264_to_h265.log' file for execution info."
echo "[$(date)]  Script send to background context. Enjoy, bye!"
# Print background PID for kill
echo "[$(date)]  ----------------------------------------------------------------------------------------------------" >> ./convert_h264_to_h265.log
echo "[$(date)]  Background Script start with PID:  $!  ('sudo kill [PID]' to terminated)" >> ./convert_h264_to_h265.log
echo "[$(date)]  ----------------------------------------------------------------------------------------------------" >> ./convert_h264_to_h265.log
#---------------------------------------------------------------------------------------------------