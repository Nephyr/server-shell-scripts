#!/bin/sh
# CLEAR ALL GENRERICS LOG
#
for CLEAN in $(find /var/log/ -type f)
do
    cp /dev/null $CLEAN
done

#
# DELETE ALL SAMBA LOGS
#
for CLEAN in $(find /var/log/samba -type f)
do
    rm -rf $CLEAN
done

#
# DELETE ALL GZ AND ROTATED FILES
#
find /var/log -type f -regex ".*\.gz$" -delete
find /var/log -type f -regex ".*\.[0-9]$" -delete
