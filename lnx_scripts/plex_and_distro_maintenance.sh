#!/bin/sh
echo " "
echo "BASHSCR: Avvio mantenimento distribuzione e server plex."
echo "PLEX-MS: Riparazione ricorsiva dei permessi di accesso al POOL in corso..."
#
cd "INSERT_PATH_TO_POOL" && chmod -R 755 "INSERT_NAME_ROOT_PLEXDIR" && chown -R plex:plex "INSERT_NAME_ROOT_PLEXDIR"
#
echo "APT-GET: Aggiornamento repositories distribuzione in corso..."
echo " "
#
apt-get update && apt-get upgrade && apt-get dist-upgrade && apt-get autoclean
#
echo " "
echo "APT-GET: Calcolo pacchetti inutilizzati in corso..."
echo " "
#
apt-get autoremove
#
echo " "
echo "BASHSCR: Mantenimento completato..."
#
read -p "BASHSCR: Vuoi terminare riavviando la macchina server? [ y | n ]: " pass
#
if test ! "$pass" = "y"
then
     echo "BASHSCR: Il sistema non verra' riavviato..."
     echo "BASHSCR: Arrivederci!"
     echo " "
else
     echo "SYSTEMD: La macchina verrà riavviata tra pochi secondi..."
     # background work
     {
          sleep 2
          #
          echo "SYSTEMD: Riavvio in corso..."
          #
          reboot
     } &
     # close all ssh sessions before restarting daemon
     kill $(ps -aux | grep ssh | grep $(last | grep "logged in" | awk '{print $2}') | awk '{print $2}')
fi
