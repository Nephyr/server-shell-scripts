#!/bin/sh
# $1 = "DIRECTORY DEST."
# $2 = "URL1,URL2,URL3,...,URLn"

for u in $2
do
    wget -b -P $1 $u --no-check-certificate
done

exit 0
