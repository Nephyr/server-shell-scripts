#!/bin/bash
# Force execution as superuser
[ "$UID" -eq 0 ] || exec sudo bash "$0" "$@"

numVars="$#"
readonly numVars

if [ "$numVars" -ne 1 ]; then
  echo "ERROR:  Arguments must be 1!"
  echo "EX:     cue2flac.sh /path/file.cue [ENTER]"
  exit 1
fi

if ! [ -e "$1" ]; then
  echo "ERROR:  $1 not found!"
  exit 1
fi

if ! [ -f "$1" ]; then
  echo "ERROR:  $1 not a file!"
  exit 1
fi

# Filenaming template:
# --------------------------------------------------
# %a author
# %A album
# %g genre
# %n track number
# %t title
# %d date
# %c comments
# --------------------------------------------------
# This is obviously the *correct* naming convetion

TEMPLATE='%n - %a - %t'

# This will check if all packages needed are present in the system, and will install them if not.

FLAC='which flac'
if [ -z "$FLAC" ]; then
  echo "WARN:  FLAC is not in your system, automatically installing..."
  sudo apt-get install flac -y
  echo "OK NOW, PROCEEDING..."
fi

CUE='which cuebreakpoints'
if [ -z "$CUE" ]; then
  echo "WARN:  Cuetools not present, automatically installing..."
  sudo apt-get install cuetools -y
  echo "OK NOW, PROCEEDING..."
fi

SHN='which shntool'
if [ -z "$SHN" ]; then
  echo "WARN:  shntool is not around here, let's get it..."
  sudo apt-get install shntool -y
  echo "OK NOW, PROCEEDING..."
fi

LL='which lltag'
if [ -z "$LL" ]; then
  echo "WARN:  lltag is not in your computer, installing..."
  sudo apt-get install lltag -y
  echo "AT LAST ALL OK, START SCRIPT..."
fi

echo ""
echo "----------------------------------------------------------------------------"
echo "Start processing of specified FLAC file based on file CUE:"
echo "----------------------------------------------------------------------------"
echo ""

# Change separator line, save and change IFS
OLDIFS=$IFS
IFS=$'\n'

# OLD CMD:   sudo shnsplit -f "$1" -t %n-%t -o flac "$2"
# Processing file CUE

FILENAME="$(basename $1)"
FILENAME="${FILENAME%.[cC][uU][eE]}"

echo "INFO:  Splitting tracks from FLAC file into 'SPLIT-TRACK#N.FLAC' in progress..."
echo ""
cuebreakpoints  $FILENAME.cue | sed s/$/0/ | shnsplit -o flac $FILENAME.flac
echo ""
echo "INFO:  All temp tracks from FLAC/CUE generated!"
echo "INFO:  Adding tags from CUE to 'SPLIT-TRACK#N.FLAC' in progress..."
cuetag $FILENAME.cue split-track*.flac
echo "INFO:  All temp tracks populated with tags!"

# This will rename files using the strucure <track-number title>, the one I like,
# but it can be easyly changed using common parameters.
# Please read lltag manual for more information.

echo "INFO:  Renaming all generated files based to CUE tags..."
lltag --yes --no-tagging --rename $TEMPLATE $(ls split-track*.flac)
echo ""
echo "INFO:  All final tracks renamed from template!"

# Restore old IFS
IFS=$OLDIFS

echo ""
echo "----------------------------------------------------------------------------"
echo "Process complete!"
echo ""
echo ""
