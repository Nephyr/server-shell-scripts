#!/bin/sh
# ES:  find_ds_store.sh /path/to/use_as_root [-delete]
find $1 \( -name '.DS_Store' -or -name '._*' \) $2
